﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication2
{
   public class HttpUtil
    {
        public static string Post(string uri, string body , string connectionId)
        {
            //byte[] response = null;
            using (WebClient client = new WebClient())
            {
                client.Headers.Add("connectionid:" + connectionId);
                client.Headers.Add("content-type:" + "application/json");

                // response = client.UploadValues(uri, pairs);

               return  client.UploadString(uri, body);
            }
           // return response;
        }
       
    }
}
