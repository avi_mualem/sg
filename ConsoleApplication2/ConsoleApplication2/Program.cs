﻿using Microsoft.AspNet.SignalR.Client;
using System;

namespace ConsoleApplication2
{
    class Program
    {
        static void Main(string[] args)
        {
            //Connection to a Signalr endpoint.
            IHubProxy _hub;
            string url = @"http://localhost:57344/signalr";
            var connection = new HubConnection(url);
            _hub = connection.CreateHubProxy("MyHub");
            connection.Start().Wait();
            _hub.On("SendData", x=> Console.WriteLine("Callback was made with the followin Data : "+ x + Environment.NewLine));

            for (int i = 0; i < 3; i++)
            {
                var body = "{ \"Data\": " + i.ToString() + " }";
                Console.WriteLine("Invoking a server request #" + i.ToString() + " with the following body: " + body);
                var response = HttpUtil.Post("http://localhost:57344/api/test",
                body, connection.ConnectionId);
          
                Console.WriteLine("server response for the the request: " + response + Environment.NewLine);
                System.Threading.Thread.Sleep(1700);
            }

            Console.WriteLine("**** Done making requests... waiting for duplex calls ***** " +Environment.NewLine);
            Console.Read();
        }
    }
}
