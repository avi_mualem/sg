﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MyApplication.Responses
{
    public class NewMessageResponse
    {
        public string Result { get; set; }
    }
}