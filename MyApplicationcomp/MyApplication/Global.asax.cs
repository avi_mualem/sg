﻿using Castle.MicroKernel.Registration;
using Castle.Windsor;
using MyApplication.Bootstrapper;
using MyApplication.Controllers;
using System.Web;
using System.Web.Http;
using System.Web.Http.Dispatcher;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace MyApplication
{
    public class WebApiApplication : HttpApplication
    {
        private readonly IWindsorContainer _container;


        public WebApiApplication()
        {
            _container = new WindsorContainer();

            //Registering the Web API controller.
            _container.Register(Component.For<TestController>().LifestyleTransient());

            //Register all other dependencies.
            BootStrapperManager.Register(_container);

        }
        protected void Application_Start()
        {
            
            GlobalConfiguration.Configuration.Services.Replace(typeof(IHttpControllerActivator),new WindsorCompositionRoot(this._container));
            AreaRegistration.RegisterAllAreas();
            GlobalConfiguration.Configure(WebApiConfig.Register);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
        }

        public override void Dispose()
        {
            //Tearing down the IOC container.
            _container.Dispose();
            base.Dispose();
        }
    }
}
