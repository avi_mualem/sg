﻿using Microsoft.AspNet.SignalR;
using Microsoft.AspNet.SignalR.Hubs;
using System;
using System.Web.Http;


namespace MyApplication.Hubs
{
    public abstract class ApiControllerWithHub<THub> : ApiController
        where THub : IHub
    {
        readonly Lazy<IHubContext> _hub = new Lazy<IHubContext>(
            () => GlobalHost.ConnectionManager.GetHubContext<THub>()
        );

        protected IHubContext Hub => _hub.Value;
    }
}