﻿using Microsoft.AspNet.SignalR;
using MyApplication.Controllers;
using MyApplication.Hubs;
using MyApplication.Model;
using MyApplication.Responses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using MyApplication.Requests;
using Newtonsoft.Json;

namespace MyApplication.Controllers
{
    public class TestController : ApiControllerWithHub<MyHub>
    {
        private readonly IMessageHandler _messageHandler;
       

        public TestController(IMessageHandler messageHandler)
        {
            _messageHandler = messageHandler;
        
        }


        // GET: api/Test
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET: api/Test/5
        public string Get(int id)
        {
            return "value";
        }

        // POST: api/Test
        [HttpPost]
        public async Task<NewMessageResponse> Post([FromBody] MySampleRequest req)
        {
            var response = new NewMessageResponse();

            try
            {
                //assuming connection id header will be found for the POC.
                var connectionId = Request.Headers.GetValues("ConnectionId").First();
               

                await _messageHandler.HandleMessageAsync(new Message() {Data = req.Data }, ()=>
                {
                    Hub.Clients.Client(connectionId)
                        .SendData("hey specific client - i know your id of your connection is : " +
                                  connectionId + " this is your callback for the request that was taken care by the " +
                                  "server at :" + DateTime.UtcNow + "and had the following body " + JsonConvert.SerializeObject( req));
                });

                response.Result = "Your action created the proper event in the queue";
                return response;
            }

            catch (Exception ex)
            {
                response.Result = "Something went wrong. just because its debug mode here is your ex message: " + ex.Message;
                return response;
            }

            
        }

        // PUT: api/Test/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/Test/5
        public void Delete(int id)
        {
        }
    }
}
