﻿using System;
using System.Threading.Tasks;

namespace MyApplication.Model
{
    public interface IMessageHandler
    {

        Task HandleMessageAsync(Message message, Action acknowledgementAction);
        
    }
}