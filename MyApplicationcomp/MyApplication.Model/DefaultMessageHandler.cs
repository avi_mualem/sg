﻿using System;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using System.Timers;

namespace MyApplication.Model
{
    /// <summary>
    /// Creates a new message from a given content
    /// and generated a queue entry that reflects the business event. 
    /// </summary>
    public class DefaultMessageHandler : IMessageHandler
    {
        public async Task HandleMessageAsync(Message message, Action acknowledgementAction)
        {
            //This code simulates writes to queue engine. it just writes to a file.
            var encodedText = Encoding.Unicode.GetBytes(Environment.NewLine + DateTime.UtcNow + " " +  message.Data);

            using (var sourceStream = new FileStream(@"C:\Users\avi\MyFakeQueue.txt",
                FileMode.Append, FileAccess.Write, FileShare.None, 4096, true))
            {
                await sourceStream.WriteAsync(encodedText, 0, encodedText.Length);
            };

            //This timer was made just in order to schedule the follow up signalr call to the client after
            //we made sure the original HTTP request was finished.
            //In real life we will probably register for a proper event that represent 
            //the success of the business flow and invoke the ack method.
            var timer = new Timer();
            timer.Elapsed += (source, e)=> 
            {
                acknowledgementAction.Invoke();
                ((Timer)source).Stop();
            };

            timer.Interval = 5000;
            timer.Enabled = true;
        }
    }
}
