﻿using Castle.MicroKernel.Registration;
using Castle.Windsor;
using MyApplication.Model;

namespace MyApplication.Bootstrapper
{
    public class BootStrapperManager
    {

        public static void Register (IWindsorContainer container )
        {
            container.Register(Component.For<IMessageHandler>().ImplementedBy<DefaultMessageHandler>().LifestyleSingleton());

        }
    }
}
